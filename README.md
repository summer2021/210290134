### 目录介绍

-  /dolphinscheduler

    链接至fork的github仓库：https://github.com/Soohyeonl/dolphinscheduler  
    最新内容在dev分支上，查看时请切换分支。由于拆分合并，该分支上的最新内容只是部分代码，查看全部代码直接跳转到：https://github.com/Soohyeonl/dolphinscheduler/tree/777155ad9daf1b8891674e59c543ae09cc4f852c ，开发内容在/dolphinscheduler-graphql文件夹下  
    代码已发起PR，https://github.com/apache/dolphinscheduler/pull/6240

- /dolphinscheduler-graphql

    开发具体代码（无法单独运行，需要在dolphinscheduler整体项目中运行）

- /210290134-中期结果

    包含中期项目报告及demo视频等

- /210290134-结项成果

    包含结项报告及相关内容

- /graphql-local

    graphql-dev分支上的最新版本，便于本地运行
