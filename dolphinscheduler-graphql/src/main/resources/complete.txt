DataSource
    connectDataSource
    connectionTest
    deleteDataSource
    verifyDataSourceName
    unauthDatasource
    authedDatasource
    getKerberosStartupState

Executor
    startProcessInstance
    execute
    startCheckProcessDefinition
    
AlertPluginInstance
    createAlertPluginInstance
    updateAlertPluginInstance
    deleteAlertPluginInstance
    getAlertPluginInstance
    getAllAlertPluginInstance
    verifyAlertInstanceName
    queryAlertPluginInstanceListPaging
    
Logger
    queryLog
    downloadTaskLog
    login
    signOut
    
Monitor
    listMaster
    listWorker
    queryDatabaseState
    queryZookeeperState
    
ProcessDefinition               ----Long改用String
    createProcessDefinition
    copyProcessDefinition
    moveProcessDefinition
    verifyProcessDefinitionName
    updateProcessDefinition
    queryProcessDefinitionVersions
    switchProcessDefinitionVersion
    deleteProcessDefinitionVersion
    releaseProcessDefinition
    queryProcessDefinitionById
    queryProcessDefinitionByName
    queryProcessDefinitionList
    queryProcessDefinitionListPaging
    viewTree
    getNodeListByDefinitionCode
    getNodeListByDefinitionCodeList     ----返回值Map待解决
    deleteProcessDefinitionById
    batchDeleteProcessDefinitionByIds
    batchExportProcessDefinitionByIds   ----无返回值
    queryProcessDefinitionAllByProjectId
    
ProcessInstance
    queryProcessInstanceList
    queryTaskListByProcessId            ----t_ds_task_instance表中无process_instance_id
    updateProcessInstanc
    queryProcessInstanceById
    queryTopNLongestRunningProcessInstance  --------测试
    deleteProcessInstanceById
    querySubProcessInstanceByTaskId
    queryParentInstanceBySubId
    viewVariables
    viewTree
    batchDeleteProcessInstanceByIds
    
Project
    createProject
    updateProjects
    queryProjectById
    queryProjectListPaging
    deleteProject
    queryUnauthorizedProject
    queryAuthorizedProject
    queryProjectCreatedAndAuthorizedByUser
    importProcessDefinition                 ----文件无法实现
    queryAllProjectList
    
Queue
    createQueue
    queryQueueList
    queryQueueListPaging
    updateQueue
    verifyQueue

Resources                                   ----测试
    createDirectory
    createResource                          ----文件参数无法实现
    updateResource                          ----文件参数无法实现
    queryResourceList
    queryResourceListPaging
    deleteResource
    verifyResourceName
    queryResourceJarList
    queryResource
    viewResource
    onlineCreateResource                    ----返回值map用string代替
    updateResourceContent
    downloadResource
    createUdfFunc
    viewUIUdfFunction
    updateUdfFunc
    queryUdfFuncListPaging
    queryUdfFuncList
    verifyUdfFuncName
    deleteUdfFunc
    authorizedFile
    authorizeResourceTree
    unauthUDFFunc
    authorizedUDFFunction
    
Schedule
    createSchedule
    updateSchedule
    online
    offline
    queryScheduleListPaging
    deleteScheduleById
    queryScheduleList
    previewSchedule
    
TaskInstance
    queryTaskListPaging
    forceTaskSuccess
    
Tenant
    createTenant
    queryTenantlistPaging
    queryTenantlist
    updateTenant
    deleteTenantById
    verifyTenantCode
    
UiPlugin
    queryUiPluginsByType
    queryUiPluginDetailById
    
User
    createUser
    queryUserList
    updateUser
    delUserById
    grantProject
    grantResource
    grantUDFFunc
    grantDataSource
    getUserInfo
    listUser
    listAll
    verifyUserName
    unauthorizedUser
    authorizedUser
    registerUser
    activateUser
    batchActivateUser
    
WorkGroup
    saveWorkerGroup
    queryAllWorkerGroupsPaging
    queryAllWorkerGroups
    deleteWorkGroupById
    queryWorkerAddressList
    
WorkFlowLineage
    queryWorkFlowLineageByName
    queryWorkFlowLineageByIds
