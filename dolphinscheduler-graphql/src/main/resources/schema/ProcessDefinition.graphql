extend type Query {
    # ProcessDefinition Query
    copyProcessDefinition(
        loginUser: InputUser!,
        projectCode: String!,
        codes: String!
        targetProjectCode: String!
    ): NormalResultType

    moveProcessDefinition(
        loginUser: InputUser!,
        projectCode: String!,
        codes: String!
        targetProjectCode: String!
    ): NormalResultType

    verifyProcessDefinitionName(
        loginUser: InputUser!,
        projectCode: String!,
        name: String!
    ): NormalResultType

    queryProcessDefinitionVersions(
        loginUser: InputUser!,
        projectCode: String!,
        pageNo: Int!,
        pageSize: Int!,
        code: String!
    ): ProcessDefinitionLogPageInfoResultType

    queryProcessDefinitionByCode(
        loginUser: InputUser!,
        projectCode: String!,
        code: String!
    ): ProcessDefinitionResultType

    queryProcessDefinitionByName(
        loginUser: InputUser!,
        projectCode: String!,
        name: String!
    ): ProcessDefinitionResultType

    queryProcessDefinitionList(
        loginUser: InputUser!,
        projectCode: String!
    ): ProcessDefinitionListResultType

    queryProcessDefinitionListPaging(
        loginUser: InputUser!,
        projectCode: String!,
        searchVal: String,
        userId: Int = 0,
        pageNo: Int!,
        pageSize: Int!
    ): ProcessDefinitionPageInfoResultType

    viewTree(
        loginUser: InputUser!,
        projectName: String!,
        id: Int!,
        limit: Int!
    ): TreeViewDtoResultType

    getNodeListByDefinitionCode(
        loginUser: InputUser!,
        projectCode: String!,
        code: String!
    ): TaskNodeListResultType

    getNodeListMapByDefinitionCodes(
        loginUser: InputUser!,
        projectCode: String!,
        codes: String!
    ): NormalResultType

    queryProcessDefinitionAllByProjectId(
        loginUser: InputUser!,
        projectId: Int!
    ): ProcessDefinitionListResultType

    queryAllProcessDefinitionByProjectCode(
        loginUser: InputUser!,
        projectCode: String!
    ): DagDataResultType

}

extend type Mutation {
    # ProcessDefinition Mutation
    createProcessDefinition(
        loginUser: InputUser!,
        projectCode: String!
        name: String!,
        description: String,
        globalParams: String = "[]",
        locations: String,
        timeout: Int = 0,
        tenantCode: String!,
        taskRelationJson: String!,
        taskDefinitionJson: String!
    ): NormalResultType

    updateProcessDefinition(
        loginUser: InputUser!,
        projectCode: String!,
        code: String!,
        description: String,
        globalParams: String = "[]",
        locations: String,
        timeout: Int = 0,
        tenantCode: String!,
        taskRelationJson: String!,
        taskDefinitionJson: String!,
        releaseState: ReleaseState = OFFLINE
    ): NormalResultType

    switchProcessDefinitionVersion(
        loginUser: InputUser!,
        projectCode: String!,
        code: String!,
        version: Int!
    ): NormalResultType

    deleteProcessDefinitionVersion(
        loginUser: InputUser!,
        projectCode: String!,
        code: String!,
        version: Int!
    ): NormalResultType

    releaseProcessDefinition(
        loginUser: InputUser!,
        projectCode: String!,
        code: String!,
        releaseState: ReleaseState!
    ): NormalResultType

    deleteProcessDefinitionByCode(
        loginUser: InputUser!,
        projectCode: String!,
        code: String!
    ): NormalResultType

    batchDeleteProcessDefinitionByCodes(
        loginUser: InputUser!,
        projectCode: String!,
        codes: String!
    ): NormalResultType
}

enum ReleaseState {
    OFFLINE
    ONLINE
}

type ProcessDefinition {
    id: Int
    code: String
    name: String
    version: Int
    releaseState: ReleaseState
    projectId: Int
    projectCode: String
    processDefinitionJson: String
    description: String
    globalParams: String
    globalParamList: [Property]
    globalParamMap: String
    createTime: String
    updateTime: String
    flag: Flag
    userId: Int
    userName: String
    projectName: String
    locations: String
    connects: String
    scheduleReleaseState: ReleaseState
    timeout: Int
    tenantId: Int
    modifyBy: String
    resourceIds: String
    warningGroupId: Int
}

type ProcessDefinitionResultType {
    code: Int
    msg: String
    data: ProcessDefinition
    success: Boolean
    failed: Boolean
}

type ProcessDefinitionListResultType {
    code: Int
    msg: String
    data: [ProcessDefinition]
    success: Boolean
    failed: Boolean
}

type ProcessDefinitionPageInfo {
    totalList: [ProcessDefinition]
    total: Int
    currentPage: Int
    totalPage: Int
}

type ProcessDefinitionPageInfoResultType {
    code: Int
    msg: String
    data: ProcessDefinitionPageInfo
    success: Boolean
    failed: Boolean
}

type Property {
    prop: String
    direct: Direct
    type: DataType
    value: String
}

enum Direct {
    IN
    OUT
}

enum DataType {
    VARCHAR,INTEGER,LONG,FLOAT,DOUBLE,DATE,TIME,TIMESTAMP,BOOLEAN,LIST
}

type ProcessDefinitionLog {
    operator: Int
    operateTime: String
    id: Int
    code: String
    name: String
    version: Int
    releaseState: ReleaseState
    projectId: Int
    projectCode: String
    processDefinitionJson: String
    description: String
    globalParams: String
    globalParamList: [Property]
    globalParamMap: String
    createTime: String
    updateTime: String
    flag: Flag
    userId: Int
    userName: String
    projectName: String
    locations: String
    connects: String
    scheduleReleaseState: ReleaseState
    timeout: Int
    tenantId: Int
    modifyBy: String
    resourceIds: String
    warningGroupId: Int
}

type ProcessDefinitionLogPageInfo {
    totalList: [ProcessDefinitionLog]
    total: Int
    currentPage: Int
    totalPage: Int
}

type ProcessDefinitionLogPageInfoResultType {
    code: Int
    msg: String
    data: ProcessDefinitionLogPageInfo
    success: Boolean
    failed: Boolean
}

type TreeViewDto {
    name: String
    type: String
    instances: [Instance]
    children: [TreeViewDto]
}

type Instance {
    id: Int
    name: String
    type: String
    state: String
    startTime: String
    endTime: String
    host: String
    duration: String
    subflowId: Int
}

type TreeViewDtoResultType {
    code: Int
    msg: String
    data: TreeViewDto
    success: Boolean
    failed: Boolean
}

type TaskNode {
    id: String
    code: String
    version: Int
    name: String
    desc: String
    type: String
    runFlag: String
    loc: String
    maxRetryTimes: Int
    retryInterval: Int
    params: String
    preTasks: String
    preTaskNodeList: [PreviousTaskNode]
    extras: String
    depList: [String]
    dependence: String
    conditionResult: String
    taskInstancePriority: Priority
    workerGroup: String
    timeout: String
    delayTime: Int
}

type PreviousTaskNode {
    code: String
    name: String
    version: Int
}

type TaskNodeListResultType {
    code: Int
    msg: String
    data: [TaskNode]
    success: Boolean
    failed: Boolean
}

type DagData {
    processDefinition: ProcessDefinition
    processTaskRelationList: [ProcessTaskRelationLog]
    taskDefinitionList: [TaskDefinitionLog]
}

type ProcessTaskRelationLog {
    id: Int
    name: String
    processDefinitionVersion: Int
    projectCode: String
    processDefinitionCode: String
    preTaskCode: String
    preTaskVersion: Int
    postTaskCode: String
    postTaskVersion: Int
    conditionType: ConditionType
    conditionParams: String
    createTime: String
    updateTime: String
    operator: Int
    operateTime: String
}

enum ConditionType {
    NONE
    JUDGE
    DELAY
}

type TaskDefinitionLog {
    id: Int
    code: String
    name: String
    version: Int
    description: String
    projectCode: String
    userId: Int
    taskType: String
    taskParams: String
    taskParamList: [Property]
    taskParamMap: String
    flag: Flag
    taskPriority: Priority
    userName: String
    projectName: String
    workerGroup: String
    environmentCode: String
    failRetryTimes: Int
    failRetryInterval: Int
    timeoutFlag: TimeoutFlag
    timeoutNotifyStrategy: TaskTimeoutStrategy
    timeout: Int
    delayTime: Int
    resourceIds: String
    createTime: String
    updateTime: String
    modifyBy: String
    operator: Int
    operateTime: String
}

type DagDataResultType {
    code: Int
    msg: String
    data: [DagData]
    success: Boolean
    failed: Boolean
}
