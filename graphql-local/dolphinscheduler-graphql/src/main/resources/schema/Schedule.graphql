extend type Query {
    # Schedule Query
    queryScheduleListPaging(
        loginUser: InputUser!,
        projectCode: String!,
        processDefinitionCode: String!,
        searchVal: String,
        pageNo: Int!,
        pageSize: Int!
    ): SchedulePageInfoResultType

    queryScheduleList(
        loginUser: InputUser!,
        projectCode: String!
    ): ScheduleListResultType

    previewSchedule(
        loginUser: InputUser!,
        schedule: String!
    ): NormalResultType
}

extend type Mutation {
    # Schedule Mutation
    createSchedule(
        loginUser: InputUser!,
        projectCode: String!,
        processDefinitionCode: String!,
        schedule: String!,
        warningType: WarningType = NONE,
        warningGroupId: Int = 1,
        failureStrategy: FailureStrategy = CONTINUE,
        workerGroup: String = "default",
        environmentCode: String = "-1",
        processInstancePriority: Priority = MEDIUM
    ): ScheduleResultType

    updateSchedule(
        loginUser: InputUser!,
        projectCode: String!,
        id: Int!,
        schedule: String!,
        warningType: WarningType = NONE,
        warningGroupId: Int,
        failureStrategy: FailureStrategy = END,
        workerGroup: String = "default",
        environmentCode: String = "-1",
        processInstancePriority: Priority
    ): NormalResultType

    online(
        loginUser: InputUser!,
        projectCode: String!,
        id: Int!
    ): NormalResultType

    offline(
        loginUser: InputUser!,
        projectCode: String!,
        id: Int!
    ): NormalResultType

    deleteScheduleById(
        loginUser: InputUser!,
        projectCode: String!,
        id: Int!
    ): NormalResultType
}

type Schedule {
    id: Int
    processDefinitionId: Int
    processDefinitionName: String
    projectName: String
    definitionDescription: String
    startTime: String
    endTime: String
    timezoneId: String
    crontab: String
    failureStrategy: FailureStrategy
    warningType: WarningType
    createTime: String
    updateTime: String
    userId: Int
    userName: String
    releaseState: ReleaseState
    warningGroupId: Int
    processInstancePriority: Priority
    workerGroup: String
}

type ScheduleResultType {
    code: Int
    msg: String
    data: Schedule
    success: Boolean
    failed: Boolean
}

type SchedulePageInfo {
    totalList: [Schedule]
    total: Int
    currentPage: Int
    totalPage: Int
}

type SchedulePageInfoResultType {
    code: Int
    msg: String
    data: SchedulePageInfo
    success: Boolean
    failed: Boolean
}

type ScheduleListResultType {
    code: Int
    msg: String
    data: [Schedule]
    success: Boolean
    failed: Boolean
}
